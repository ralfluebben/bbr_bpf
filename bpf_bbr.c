// SPDX-License-Identifier: GPL-2.0-only

/* WARNING: This implemenation is not necessarily the same
 * as the tcp_cubic.c.  The purpose is mainly for testing
 * the kernel BPF logic.
 *
 * Highlights:
 * 1. CONFIG_HZ .kconfig map is used.
 * 2. In bictcp_update(), calculation is changed to use usec
 *    resolution (i.e. USEC_PER_JIFFY) instead of using jiffies.
 *    Thus, usecs_to_jiffies() is not used in the bpf_cubic.c.
 * 3. In bitctcp_update() [under tcp_friendliness], the original
 *    "while (ca->ack_cnt > delta)" loop is changed to the equivalent
 *    "ca->ack_cnt / delta" operation.
 */

#include "vmlinux.h" // generated with bpftool btf dump file /sys/kernel/btf/vmlinux format c > vmlinux.h
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_core_read.h>
#include <bpf/bpf_tracing.h>
#include <bpf/bpf_core_read.h>
#include <bpf/bpf_endian.h>

char _license[] SEC("license") = "GPL";

#ifndef memset
#define memset(dest, chr, n)   __builtin_memset((dest), (chr), (n))
#endif

#ifndef likely
#define likely(x) __builtin_expect(!!(x), 1)
#endif

// #define BPF_STRUCT_OPS(name, args...) \
// SEC("struct_ops/"#name) \
// BPF_PROG(name, args)

#define tcp_jiffies32 ((__u32)bpf_jiffies64())
#define USEC_PER_SEC	1000000ULL

#define	TCP_ECN_OK		1
#define	TCP_ECN_QUEUE_CWR	2
#define	TCP_ECN_DEMAND_CWR	4
#define	TCP_ECN_SEEN		8

#define TCPI_OPT_TIMESTAMPS	1
#define TCPI_OPT_SACK		2
#define TCPI_OPT_WSCALE		4
#define TCPI_OPT_ECN		8 /* ECN was negociated at TCP session init */
#define TCPI_OPT_ECN_SEEN	16 /* we received at least one packet with ECT */
#define TCPI_OPT_SYN_DATA	32 /* SYN-ACK acked data in SYN sent or rcvd */

#define BW_SCALE 24

#define inet_dport		sk.__sk_common.skc_dport
#define sk_cookie		sk.__sk_common.skc_cookie
#define inet_daddr		sk.__sk_common.skc_daddr

extern unsigned CONFIG_HZ __kconfig;

static __always_inline struct inet_sock *inet_sk(const struct sock *sk)
{
	return (struct inet_sock *)sk;
}

static __always_inline struct inet_connection_sock *inet_csk(const struct sock *sk)
{
	return (struct inet_connection_sock *)sk;
}

static __always_inline void *inet_csk_ca(const struct sock *sk)
{
	return (void *)inet_csk(sk)->icsk_ca_priv;
}

static __always_inline struct tcp_sock *tcp_sk(const struct sock *sk)
{
	return (struct tcp_sock *)sk;
}

static inline __u32 minmax_get(const struct minmax *m)
{
	return m->s[0].v;
}

/* Return the windowed max recent bandwidth sample, in pkts/uS << BW_SCALE. */
static __u32 bbr_max_bw(const struct sock *sk)
{
	struct bbr *bbr = inet_csk_ca(sk);

	return minmax_get(&bbr->bw);
}

/* Return the estimated bandwidth of the path, in pkts/uS << BW_SCALE. */
static __u32 bbr_bw(const struct sock *sk)
{
	struct bbr *bbr = inet_csk_ca(sk);

	return bbr->lt_use_bw ? bbr->lt_bw : bbr_max_bw(sk);
}

static inline __u64 tcp_compute_delivery_rate(const struct tcp_sock *tp)
{
	__u32 rate = tp->rate_delivered;
	__u32 intv = tp->rate_interval_us;
	__u64 rate64 = 0;

	if (rate && intv) {
		rate64 = (__u64)rate * tp->mss_cache * USEC_PER_SEC;
		rate64 = rate64/intv;
	}
	return rate64;
}

unsigned int jiffies_to_usecs(const unsigned long j)
{
	return (USEC_PER_SEC / CONFIG_HZ) * j;
}


static inline int tcp_is_sack(const struct tcp_sock *tp)
{
	return likely(tp->rx_opt.sack_ok);
}

static inline __u32 tcp_min_rtt(const struct tcp_sock *tp)
{
	return minmax_get(&tp->rtt_min);
}

static void tcp_get_info_chrono_stats(const struct tcp_sock *tp,
				      struct tcp_info *info)
{
	__u64 stats[__TCP_CHRONO_MAX], total = 0;
	enum tcp_chrono i;

	for (i = TCP_CHRONO_BUSY; i < __TCP_CHRONO_MAX; ++i) {
		stats[i] = tp->chrono_stat[i - 1];
		if (i == tp->chrono_type)
			stats[i] += tcp_jiffies32 - tp->chrono_start;
		stats[i] *= USEC_PER_SEC / CONFIG_HZ;
		total += stats[i];
	}

	info->tcpi_busy_time = total;
	info->tcpi_rwnd_limited = stats[TCP_CHRONO_RWND_LIMITED];
	info->tcpi_sndbuf_limited = stats[TCP_CHRONO_SNDBUF_LIMITED];
}

static size_t bbr_get_info(struct sock *sk, struct tcp_bbr_info	* bbr_info)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct bbr *bbr = inet_csk_ca(sk);
	u64 bw = bbr_bw(sk);

	bw = bw * tp->mss_cache * USEC_PER_SEC >> BW_SCALE;
	memset(bbr_info, 0, sizeof(*bbr_info));
	bbr_info->bbr_bw_lo		= (u32)bw;
	bbr_info->bbr_bw_hi		= (u32)(bw >> 32);
	bbr_info->bbr_min_rtt		= bbr->min_rtt_us;
	bbr_info->bbr_pacing_gain	= bbr->pacing_gain;
	bbr_info->bbr_cwnd_gain		= bbr->cwnd_gain;
	return sizeof(*bbr_info);
}

static inline void tcp_get_info(struct sock *sk, struct tcp_info *info)
{
	const struct tcp_sock *tp = tcp_sk(sk); /* iff sk_type == SOCK_STREAM */
	const struct inet_connection_sock *icsk = inet_csk(sk);
	unsigned long rate;
	__u32 now;
	__u64 rate64;

	memset(info, 0, sizeof(*info));

	info->tcpi_state = sk->__sk_common.skc_state;

	/* Report meaningful fields for all TCP states, including listeners */
	rate = sk->sk_pacing_rate;
	rate64 = (rate != ~0UL) ? rate : ~0ULL;
	info->tcpi_pacing_rate = rate64;

	rate = sk->sk_max_pacing_rate;
	rate64 = (rate != ~0UL) ? rate : ~0ULL;
	info->tcpi_max_pacing_rate = rate64;

	info->tcpi_reordering = tp->reordering;
	info->tcpi_snd_cwnd = tp->snd_cwnd;

	if (info->tcpi_state == TCP_LISTEN) {
		/* listeners aliased fields :
		 * tcpi_unacked -> Number of children ready for accept()
		 * tcpi_sacked  -> max backlog
		 */
		info->tcpi_unacked = sk->sk_ack_backlog;
		info->tcpi_sacked = sk->sk_max_ack_backlog;
		return;
	}

	info->tcpi_ca_state = icsk->icsk_ca_state;
	info->tcpi_retransmits = icsk->icsk_retransmits;
	info->tcpi_probes = icsk->icsk_probes_out;
	info->tcpi_backoff = icsk->icsk_backoff;
	
	if (tp->rx_opt.tstamp_ok)
		info->tcpi_options |= TCPI_OPT_TIMESTAMPS;
	if (tcp_is_sack(tp))
		info->tcpi_options |= TCPI_OPT_SACK;
	if (tp->rx_opt.wscale_ok) {
		info->tcpi_options |= TCPI_OPT_WSCALE;
		info->tcpi_snd_wscale = tp->rx_opt.snd_wscale;
		info->tcpi_rcv_wscale = tp->rx_opt.rcv_wscale;
	}

	if (tp->ecn_flags & TCP_ECN_OK)
		info->tcpi_options |= TCPI_OPT_ECN;
	if (tp->ecn_flags & TCP_ECN_SEEN)
		info->tcpi_options |= TCPI_OPT_ECN_SEEN;
	if (tp->syn_data_acked)
		info->tcpi_options |= TCPI_OPT_SYN_DATA;
	
	info->tcpi_rto = jiffies_to_usecs(icsk->icsk_rto);
	info->tcpi_ato = jiffies_to_usecs(icsk->icsk_ack.ato);
	info->tcpi_snd_mss = tp->mss_cache;
	info->tcpi_rcv_mss = icsk->icsk_ack.rcv_mss;
	
	info->tcpi_unacked = tp->packets_out;
	info->tcpi_sacked = tp->sacked_out;

	info->tcpi_lost = tp->lost_out;
	info->tcpi_retrans = tp->retrans_out;

	now = tcp_jiffies32;
	info->tcpi_last_data_sent = jiffies_to_usecs(now - tp->lsndtime);
	info->tcpi_last_data_recv = jiffies_to_usecs(now - icsk->icsk_ack.lrcvtime);
	info->tcpi_last_ack_recv = jiffies_to_usecs(now - tp->rcv_tstamp);
	
	info->tcpi_pmtu = icsk->icsk_pmtu_cookie;
	info->tcpi_rcv_ssthresh = tp->rcv_ssthresh;
	info->tcpi_rtt = tp->srtt_us >> 3;
	info->tcpi_rttvar = tp->mdev_us >> 2;
	info->tcpi_snd_ssthresh = tp->snd_ssthresh;
	info->tcpi_advmss = tp->advmss;
	
	info->tcpi_rcv_rtt = tp->rcv_rtt_est.rtt_us >> 3;
	info->tcpi_rcv_space = tp->rcvq_space.space;

	info->tcpi_total_retrans = tp->total_retrans;
	
	info->tcpi_bytes_acked = tp->bytes_acked;
	info->tcpi_bytes_received = tp->bytes_received;
	if (tp->write_seq > tp->snd_nxt)
		info->tcpi_notsent_bytes = tp->write_seq - tp->snd_nxt;
	else
		info->tcpi_notsent_bytes = 0;
	tcp_get_info_chrono_stats(tp, info);

	info->tcpi_segs_out = tp->segs_out;
	info->tcpi_segs_in = tp->segs_in;
	
	info->tcpi_min_rtt = tcp_min_rtt(tp);
	
	info->tcpi_data_segs_in = tp->data_segs_in;
	info->tcpi_data_segs_out = tp->data_segs_out;
	
	info->tcpi_delivery_rate_app_limited = tp->rate_app_limited ? 1 : 0;
	rate64 = tcp_compute_delivery_rate(tp);
	if (rate64)
		info->tcpi_delivery_rate = rate64;
	info->tcpi_delivered = tp->delivered;
	info->tcpi_delivered_ce = tp->delivered_ce;
	info->tcpi_bytes_sent = tp->bytes_sent;
	info->tcpi_bytes_retrans = tp->bytes_retrans;
	info->tcpi_dsack_dups = tp->dsack_dups;
	info->tcpi_reord_seen = tp->reord_seen;
	info->tcpi_rcv_ooopack = tp->rcv_ooopack;
	info->tcpi_snd_cwnd = tp->snd_cwnd;
	info->tcpi_fastopen_client_fail = tp->fastopen_client_fail;
	return;
}

struct {
	__uint(type, BPF_MAP_TYPE_RINGBUF);
	__uint(max_entries, 256*4096);
	__uint(pinning, LIBBPF_PIN_BY_NAME);
} tcp_bbr_probe SEC(".maps");

struct event {
  struct tcp_info ti;
  struct tcp_bbr_info bi;
  __u64 ts;
  __u64 cookie;
  __u16 	dport;
  __be32	daddr;
  __be32	saddr;
  __be16	sport;
};

extern void bbr_init(struct sock *sk) __ksym;

SEC("struct_ops/bpf_bbr_init")
void BPF_PROG(bpf_bbr_init, struct sock *sk)
{
	bbr_init(sk);
}

static inline void process_event(struct sock *sk)
{
	const struct  inet_sock *isk =  inet_sk(sk);
	const struct tcp_sock *tp = tcp_sk(sk); /* iff sk_type == SOCK_STREAM */
	struct event *event = NULL;
	union tcp_cc_info info;

	if (bpf_ntohs(isk->inet_dport) > 0 && tp->segs_in <= 30){
		event = bpf_ringbuf_reserve(&tcp_bbr_probe, sizeof(struct event), 0);
		if (event){
				tcp_get_info(sk, &(event->ti));
				bbr_get_info(sk, &(event->bi));
				event->ts = bpf_ktime_get_ns();
				event->cookie = 0; //bpf_get_socket_cookie(sk);
				event->dport = bpf_ntohs(isk->inet_dport);
				event->sport = bpf_ntohs(isk->inet_sport);
				event->saddr = isk->inet_saddr;
				event->daddr = isk->inet_daddr;
				bpf_ringbuf_submit(event, 0);
		}
	}
}


extern void bbr_main(struct sock *sk, const struct rate_sample *rs) __ksym;

SEC("struct_ops/bpf_bbr_main")
void BPF_PROG(bpf_bbr_main, struct sock *sk, const struct rate_sample *rs)
{
	process_event(sk);
	bbr_main(sk,rs);
}

extern __u32 bbr_sndbuf_expand(struct sock *sk) __ksym;

SEC("struct_ops/bpf_bbr_sndbuf_expand")
__u32 BPF_PROG(bpf_bbr_sndbuf_expand, struct sock *sk)
{
	return bbr_sndbuf_expand(sk);
}

extern __u32 bbr_ssthresh(struct sock *sk) __ksym;

SEC("struct_ops/bpf_bbr_ssthresh")
__u32 BPF_PROG(bpf_bbr_ssthresh, struct sock *sk)
{
	return bbr_ssthresh(sk);
}

extern __u32 bbr_min_tso_segs(struct sock *sk) __ksym;

SEC("struct_ops/bpf_bbr_min_tso_segs")
__u32 BPF_PROG(bpf_bbr_min_tso_segs, struct sock *sk)
{
	return bbr_min_tso_segs(sk);
}

extern void bbr_set_state(struct sock *sk, __u8 new_state) __ksym;

SEC("struct_ops/bpf_bbr_set_state")
void BPF_PROG(bpf_bbr_set_state, struct sock *sk, __u8 new_state)
{
	bbr_set_state(sk, new_state);
}

extern __u32 bbr_undo_cwnd(struct sock *sk) __ksym;

SEC("struct_ops/bpf_bbr_undo_cwnd")
__u32 BPF_PROG(bpf_bbr_undo_cwnd, struct sock *sk)
{
	return bbr_undo_cwnd(sk);
}

extern void bbr_cwnd_event(struct sock *sk, enum tcp_ca_event event) __ksym;

SEC("struct_ops/bpf_bbr_cwnd_event")
void BPF_PROG(bpf_bbr_cwnd_event, struct sock *sk, enum tcp_ca_event event)
{
	bbr_cwnd_event(sk, event);
}

extern void tcp_reno_cong_avoid(struct sock *sk, __u32 ack, __u32 acked) __ksym;

SEC("struct_ops/no_cong_avoid")
void BPF_PROG(no_cong_avoid, struct sock *sk, __u32 ack, __u32 acked)
{
	//see net/ipv4/bpf_tcp_ca.c#L13 , only cong_control is optional, by either cong_control or cong_avoid must be implemented
	bpf_printk("BUG: cong_control is implemented cong_avoid should not be called but must be implement\n"); 
}

SEC(".struct_ops")
struct tcp_congestion_ops bbr = {
	.init		= (void *)bpf_bbr_init,
	.cong_control	= (void *)bpf_bbr_main,
	.sndbuf_expand	= (void *)bpf_bbr_sndbuf_expand,
	.undo_cwnd	= (void *)bpf_bbr_undo_cwnd,
    .cong_avoid	= (void *)no_cong_avoid,
	.cwnd_event	= (void *)bpf_bbr_cwnd_event,
	.ssthresh	= (void *)bpf_bbr_ssthresh,
	.min_tso_segs	= (void *)bpf_bbr_min_tso_segs,
	.set_state	= (void *)bpf_bbr_set_state,
	.name		= "bpf_bbr",
};