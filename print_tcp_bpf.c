#include <bpf/libbpf.h>
#include <bpf/bpf.h>
#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <openssl/sha.h>

struct tcp_info {
	__u8	tcpi_state;
	__u8	tcpi_ca_state;
	__u8	tcpi_retransmits;
	__u8	tcpi_probes;
	__u8	tcpi_backoff;
	__u8	tcpi_options;
	__u8	tcpi_snd_wscale : 4, tcpi_rcv_wscale : 4;
	__u8	tcpi_delivery_rate_app_limited:1, tcpi_fastopen_client_fail:2;

	__u32	tcpi_rto;
	__u32	tcpi_ato;
	__u32	tcpi_snd_mss;
	__u32	tcpi_rcv_mss;

	__u32	tcpi_unacked;
	__u32	tcpi_sacked;
	__u32	tcpi_lost;
	__u32	tcpi_retrans;
	__u32	tcpi_fackets;

	/* Times. */
	__u32	tcpi_last_data_sent;
	__u32	tcpi_last_ack_sent;     /* Not remembered, sorry. */
	__u32	tcpi_last_data_recv;
	__u32	tcpi_last_ack_recv;

	/* Metrics. */
	__u32	tcpi_pmtu;
	__u32	tcpi_rcv_ssthresh;
	__u32	tcpi_rtt;
	__u32	tcpi_rttvar;
	__u32	tcpi_snd_ssthresh;
	__u32	tcpi_snd_cwnd;
	__u32	tcpi_advmss;
	__u32	tcpi_reordering;

	__u32	tcpi_rcv_rtt;
	__u32	tcpi_rcv_space;

	__u32	tcpi_total_retrans;

	__u64	tcpi_pacing_rate;
	__u64	tcpi_max_pacing_rate;
	__u64	tcpi_bytes_acked;    /* RFC4898 tcpEStatsAppHCThruOctetsAcked */
	__u64	tcpi_bytes_received; /* RFC4898 tcpEStatsAppHCThruOctetsReceived */
	__u32	tcpi_segs_out;	     /* RFC4898 tcpEStatsPerfSegsOut */
	__u32	tcpi_segs_in;	     /* RFC4898 tcpEStatsPerfSegsIn */

	__u32	tcpi_notsent_bytes;
	__u32	tcpi_min_rtt;
	__u32	tcpi_data_segs_in;	/* RFC4898 tcpEStatsDataSegsIn */
	__u32	tcpi_data_segs_out;	/* RFC4898 tcpEStatsDataSegsOut */

	__u64   tcpi_delivery_rate;

	__u64	tcpi_busy_time;      /* Time (usec) busy sending data */
	__u64	tcpi_rwnd_limited;   /* Time (usec) limited by receive window */
	__u64	tcpi_sndbuf_limited; /* Time (usec) limited by send buffer */

	__u32	tcpi_delivered;
	__u32	tcpi_delivered_ce;

	__u64	tcpi_bytes_sent;     /* RFC4898 tcpEStatsPerfHCDataOctetsOut */
	__u64	tcpi_bytes_retrans;  /* RFC4898 tcpEStatsPerfOctetsRetrans */
	__u32	tcpi_dsack_dups;     /* RFC4898 tcpEStatsStackDSACKDups */
	__u32	tcpi_reord_seen;     /* reordering events seen */

	__u32	tcpi_rcv_ooopack;    /* Out-of-order packets received */

	__u32	tcpi_snd_wnd;	     /* peer's advertised receive window after
				      			  * scaling (bytes)
				      			  */
};


struct tcp_bbr_info {
	/* u64 bw: max-filtered BW (app throughput) estimate in Byte per sec: */
	__u32	bbr_bw_lo;		/* lower 32 bits of bw */
	__u32	bbr_bw_hi;		/* upper 32 bits of bw */
	__u32	bbr_min_rtt;		/* min-filtered RTT in uSec */
	__u32	bbr_pacing_gain;	/* pacing gain shifted left 8 bits */
	__u32	bbr_cwnd_gain;		/* cwnd gain shifted left 8 bits */
};


struct event {
  struct tcp_info ti;
  struct tcp_bbr_info bi;
  __u64 ts;
  __u64 cookie;
  __u16 	dport;
  __be32	daddr;
  __be32	saddr;
  __be16	sport;
};

static int buf_process_sample(void *ctx, void *data, size_t len) {
  struct event *evt = (struct event *)data;
  __u64 bw = evt->bi.bbr_bw_hi;
  bw = (bw << 32) |  evt->bi.bbr_bw_lo;

	unsigned char ibuf[4+4+2+2];
	unsigned char hash_id[32] ;

	memcpy(ibuf, &(evt->dport), 2);
	memcpy(ibuf+2, &(evt->daddr), 4);
	memcpy(ibuf+4, &(evt->saddr), 4);
	memcpy(ibuf+4, &(evt->sport), 2);

    SHA256(ibuf, 12, hash_id);
	
	for (int i = 0; i < 8; i++) {
        printf("%02x", hash_id[i]);
    }
   printf(" ");	
  printf("%llu %u %u %u %u %llu %u %llu %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %u %llu %llu %u %u %u %llu %llu %llu %u %u %u %llu %u %u %llu %llu %u %u %u %u %llu %u %u %u\n",
	evt->ts,
	0, //evt->saddr,
	0, //evt->sport,
    0, //evt->daddr,
	0, //evt->dport,
	evt->cookie,
  	evt->ti.tcpi_state, 
	evt->ti.tcpi_pacing_rate/1000000*8, 
	evt->ti.tcpi_snd_cwnd, 
	evt->ti.tcpi_min_rtt, 
	evt->ti.tcpi_ca_state, 
	evt->ti.tcpi_retransmits, 
	evt->ti.tcpi_probes,
	evt->ti.tcpi_backoff,
	evt->ti.tcpi_rto,
	evt->ti.tcpi_ato,
	evt->ti.tcpi_last_data_sent,
	evt->ti.tcpi_last_data_recv,
	evt->ti.tcpi_last_ack_recv,
	evt->ti.tcpi_pmtu,
	evt->ti.tcpi_rcv_ssthresh,
	evt->ti.tcpi_rtt,
	evt->ti.tcpi_rttvar,
	evt->ti.tcpi_snd_ssthresh,
	evt->ti.tcpi_advmss,
	evt->ti.tcpi_rcv_rtt,
	evt->ti.tcpi_rcv_space,
	evt->ti.tcpi_total_retrans,

	evt->ti.tcpi_bytes_acked,
	evt->ti.tcpi_bytes_received,
	evt->ti.tcpi_notsent_bytes,
	evt->ti.tcpi_segs_out,
	evt->ti.tcpi_segs_in,
	evt->ti.tcpi_busy_time,
	evt->ti.tcpi_rwnd_limited,
	evt->ti.tcpi_sndbuf_limited,
	//%llu %llu %u %u %u %llu %llu %llu
	evt->ti.tcpi_data_segs_in,
	evt->ti.tcpi_data_segs_out,
	evt->ti.tcpi_delivery_rate_app_limited,
	evt->ti.tcpi_delivery_rate,
	evt->ti.tcpi_delivered,
	evt->ti.tcpi_delivered_ce,
	evt->ti.tcpi_bytes_sent,
	evt->ti.tcpi_bytes_retrans,
	evt->ti.tcpi_dsack_dups,
	evt->ti.tcpi_reord_seen,
	evt->ti.tcpi_rcv_ooopack,
	evt->ti.tcpi_fastopen_client_fail,
	//%u %u %u %u %u %u %llu %llu %u %u %u %u
	bw,
	evt->bi.bbr_min_rtt,
	evt->bi.bbr_pacing_gain,
	evt->bi.bbr_cwnd_gain
	//%llu %u %u %u
	);
  return 0;
}

int main(int argc, char *argv[]) {
  int buffer_map_fd = -1;
  struct ring_buffer *ring_buffer;
   
  buffer_map_fd = bpf_obj_get("/sys/fs/bpf/tcp_bbr_probe");
 
  ring_buffer = ring_buffer__new(buffer_map_fd, buf_process_sample, NULL, NULL);

  if(!ring_buffer) {
    fprintf(stderr, "failed to create ring buffer\n");
    return 1;
  }

  while(1) {
	  int err = ring_buffer__poll(ring_buffer, 100 );
 		/* Ctrl-C will cause -EINTR */
 		//should be EINTR, include correct header
		if (err == -4) {
 			err = 0;
 			break;
 		}
 		if (err < 0) {
			printf("Error polling ring buffer: %d\n", err);
					break;
		}
  }
  return 0;
}
