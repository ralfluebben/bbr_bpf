# bbr_bpf

## Prerequirements
For eBPF the bpftool and and related libraries are required, contained among others in the linux-tools package:
```
sudo apt install linux-tools-common 
sudo apt install linux-tools-generic linux-cloud-tools-generic
sudo apt install clang
sudo apt install libbpf-dev
sudo apt install libbpf0
```

Since we rely on BBR kernel functions, we have to install TCP BBR as built-in not as kernel modul, otherwise we get the error
```
libbpf: elf: skipping unrecognized data section(25) .rodata.str1.1
libbpf: extern (func ksym) 'bbr_cwnd_event': function in kernel module is not supported
libbpf: failed to load object './bpf_bbr.o'
```

## Rebuild kernel

Enable the src repositories in `/etc/apt/sources` and run
```
sudo apt update
```
Get the code of the current unsigned kernel
```
apt-get source linux-image-unsigned-$(uname -r)
```

Install build environment see https://wiki.ubuntu.com/Kernel/BuildYourOwnKernel
```
sudo apt-get build-dep linux linux-image-$(uname -r)
sudo apt-get install libncurses-dev gawk flex bison openssl libssl-dev dkms libelf-dev libudev-dev libpci-dev libiberty-dev autoconf
sudo apt-get install kernel-wedge llvm libzstd1 libzstd-dev libssl-dev libssl llvm
```

Change into the kernel source and edit the config
```
chmod a+x debian/rules
chmod a+x debian/scripts/*
chmod a+x debian/scripts/misc/*
LANG=C fakeroot debian/rules clean
LANG=C fakeroot debian/rules editconfigs
```

Modifiy `debian.master/config/annotations` to 
```
CONFIG_TCP_CONG_BBR                             policy<{'amd64': 'y', 'arm64': 'm', 'armhf': 'm', 'ppc64el': 'm', 's390x': 'm'}>
```
so that is inline with the new kernel config.

Build with
```
LANG=C fakeroot debian/rules clean
# quicker build:
DEB_BUILD_OPTIONS=parallel=48 AUTOBUILD=1 NOEXTRAS=1 CONCURRENCY_LEVEL=48 LANG=C fakeroot debian/rules binary-headers binary-generic binary-perarch
```
and install the kernel with `dpkg`.

## Compile the bpf programm
```
bpftool btf dump file /sys/kernel/btf/vmlinux format c > vmlinux.h
clang  -g -D__TARGET_ARCH_x86 -mlittle-endian -Wno-compare-distinct-pointer-types -DENABLE_ATOMICS_TESTS -O2 -target bpf -c bpf_bbr.c -o bpf_bbr.o -mcpu=v3
```

## Register the programm and the congestion control algorithm

```
sudo bpftool struct_ops register ./bpf_bbr.o
sudo sysctl -w net.ipv4.tcp_congestion_control="bpf_bbr"
```

## Compile and start the user space app
```
gcc -g -rdynamic -Wall -O2 -DHAVE_GENHDR  print_tcp_bpf.c -lbpf -lcrypto -o print_tcp_bpf
sudo ./print_tcp_bpf
```

## Unregister the programm and the congestion control algorithm
```
sudo sysctl -w net.ipv4.tcp_congestion_control="bbr"
sudo bpftool struct_ops list
sudo bpftool struct_ops unregister id <id>
```

## Sock cookie 
Currently, the sock cookie is set to 0. Since the function `bpf_get_socket_cookie(sk)` is not available for `struct_ops`.

To make it available, the kernel has to be modified. In detail the file `net/ipv4/bpf_tcp_ca.c` has to modified to 

```
extern const struct bpf_func_proto bpf_get_socket_ptr_cookie_proto;

static const struct bpf_func_proto *
bpf_tcp_ca_get_func_proto(enum bpf_func_id func_id,
                          const struct bpf_prog *prog)
{
...
        case BPF_FUNC_get_socket_cookie:
                return &bpf_get_socket_ptr_cookie_proto;
...
}
```

